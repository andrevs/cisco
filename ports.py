#!/usr/bin/env python

# IOS - access port
def ios(list, date):
      print 'interface %s' % list[2]
      print 'description %s, %s, %s, %s, Ref#%s, %s, WCollins' % (list[0], list[1], list[2], list[3], list[7], date)
      print 'switchport'
      print 'switchport access vlan %s' % list[4]
      print 'switchport mode access'
      print 'no ip address'
      print 'speed %s' % list[5]
      print 'duplex %s' % list[6]
      print 'spanning-tree portfast'
      print 'no shutdown'
      print 'exit'
      print '!'

# Nexus - access port
def nexus(list, date):
      print 'interface %s' % list[2]
      print 'description %s, %s, %s, %s, Ref#%s, %s, WCollins' % (list[0], list[1], list[2], list[3], list[7], date)
      print 'switchport access vlan %s' % list[4]
      print 'spanning-tree port type edge'
      print 'spanning-tree guard root'
      print 'speed %s' % list[5]
      print 'duplex %s' % list[6]
      print 'no shutdown'
      print 'exit'
      print '!'

# Main logic
def main():
    data = 'ports.csv'
    date = time.strftime("%m/%d/%Y")
    option = raw_input('\n- IOS: 1 \n- NX-OS: 2\n\nSelection# ')

    # Loop & generate IOS access ports
    if option == "1":
        with open(data, 'rb') as inData:
            data = csv.reader(inData)
            next(data, None)
            for row in data:
                list = row
                ios(list, date)

    # Loop & generate NX-OS access ports
    if option == "2":
        with open(data, 'rb') as inData:
            data = csv.reader(inData)
            next(data, None)
            for row in data:
                list = row
                ios(list, date)

if __name__ == "__main__":

      import sys, csv, time

      main()
